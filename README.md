# Lab 8

https://bitbucket.org/elissa_skinner1/lab-8/src/master/

1. logger.log is at a lower level than the log to console so it has unnecessary details from the core libraries printed to it.
2. This line comes from the logger within JUnit that checks if a test is enabled. In this case the test is enabled.
3. Assertions.assertThrows calls a method and watches for the method to throw the specificed exception. If the exception is thrown then the test passes, otherwise it fails.
4. TimerException
    1. serialVersionUID is used to verify between two different users that an object is from the same class.
    2. It is necessary to make a new constructor for the child class because you cannot inherit a constructor. You must make a constructor that calls super to to call the parent constructor.
    3. The other exception methods are inherited and thus can be called without being overriden. Since there were no changes that needed to be made to the other exceptions in TimerException class they were not overriden.
5. The static block static {} runs the when the first Timer object is created. It runs before anything else does but only runs once. It configures the logger for the class.
6. README.md uses Markdown syntax. bitbucket is able display Markdown README files by hitting Control-Shift-P.
7. A TimerException is thrown but before the catch block is called the finally block is called. Within the finally block a nullptrException occurs since timeNow is initially assigned to null. This causes a nullptrException to be returned instead of a TimerException so the test fails. This can be fixed by initializing timeNow to 0.
8. First a TimerException is thrown. Next the finally block is ran. Inside the finally block a nullptrException occurs. The method then ends and the test fails.
11. TimerException is a checked exception. This means that it is checked at compile time. NullPointerException is an unchecked exception. It is not caught within the program and is not checked at compile time.
